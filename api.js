//Form
const ciudad = document.getElementById('ciudad')
const buscar = document.getElementById('buscar')

//Datos:
const nombreCiudad = document.getElementById('nombre_ciudad')
const clima = document.getElementById('clima')
const temperatura = document.getElementById('temperatura')
const humedad = document.getElementById('humedad')
const dia = document.getElementById('dia')
const icon = document.getElementById('icon')


//CONSULTA A LA API
const llamadaClima = (ciudad) => {
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${ciudad}&units=metric&appid=2c376b84a5eae1ed66538d8d10854ad7`) //consulta a la api
    .then(res => res.ok ? Promise.resolve(res) : Promise.reject(res))
    .then(res => res.json())
    .then(res => {
        nombreCiudad.innerHTML = `${res.name}`
        clima.innerHTML = `${res.weather[0].description}`
        temperatura.innerHTML = `${res.main.temp} &#176;C`
        humedad.innerHTML = `Humedad ${res.main.humidity} %`
        console.log(res)
    })
}

//llamadaClima('Buenos Aires')
buscar.addEventListener('click', (e) => {
    e.preventDefault()
    llamadaClima(ciudad.value)
})
ciudad.addEventListener('keydown', (e) => {
    if(e.code == 'Enter') {
        llamadaClima(ciudad.value)
    }
    //llamadaClima(ciudad.value)
})
